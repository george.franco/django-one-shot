from multiprocessing import context
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from todos.models import TodoItem, TodoList

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     print(context)
    #     return context


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    # model context is read in detail as todolist

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     print(context)
    #     return context


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list_detail")


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list_detail")


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = ["task"]
    template_name = "todos/new_item.html"
    success_url = reverse_lazy("todo_list_detail")
