from django.urls import path

from todos.views import (
    TodoListView,
    TodoDetailView,
    TodoCreateView,
    TodoDeleteView,
    TodoUpdateView,
)

urlpatterns = [
    path("", TodoListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoCreateView.as_view(), name="todo_list_create"),
    path("<int:pk>/delete", TodoDeleteView.as_view(), name="todo_list_delete"),
    path("<int:pk>/edit", TodoUpdateView.as_view(), name="todo_list_update"),
]
